
package com.example.demo.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployee() {
        return employeeRepository.findAll();
    }

    public void registerNewEmployee(Employee employee) {
        Optional<Employee> optionalEmployee=employeeRepository.findEmployeeByEmail(employee.getEmail());
        if(optionalEmployee.isPresent())
        {
            throw new IllegalArgumentException("email is taken already");
        }
        //employeeRepository.save(employee);
        System.out.println(employee);
    }

    public void deleteEmployee(Long id) {
        boolean exists=employeeRepository.existsById(id);
        if(!exists)
        {
            throw new IllegalArgumentException("Id not Exists");
        }
        employeeRepository.deleteById(id);
    }

    public void updateEmployee(Long id, String first_name, String last_name, String email) {

        Optional<Employee> optional=employeeRepository.findById(id);
        if(optional.isEmpty())
        {
            throw new IllegalArgumentException("Id not Exists");
        }
        Employee e = optional.get();

        if(first_name != null && first_name.length()>0 )
        {
            e.setFirst_name(first_name);
        }
        if(last_name != null && last_name.length()>0 )
        {
            e.setLast_name(last_name);
        }
        if(email != null && email.length()>0 )
        {
            e.setEmail(email);
        }
        employeeRepository.save(e);
    }
}
