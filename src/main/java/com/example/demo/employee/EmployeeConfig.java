package com.example.demo.employee;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class EmployeeConfig {

//    @Bean // because it will made object of this
//    CommandLineRunner commandLineRunner(EmployeeRepository empRepository){
//        return args -> {
//            Employee emp1=new Employee(
//                    1L,
//                    "vinay",
//                    "gupta",
//                    LocalDate.of(2000,Month.JANUARY,03),
//                    "fff@gmail.com",
//                    21
//            );
//             Employee emp2=new Employee(
//                     2L,
//                     "hello",
//                     "world",
//                     LocalDate.of(2000,Month.APRIL,2),
//                     "ddd",
//                     34
//             );
//             empRepository.saveAll(List.of(emp1,emp2));
//        };
//    }
     //commandLineRunner executes after starting of SpringBoot Application
}
