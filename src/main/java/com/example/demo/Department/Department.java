package com.example.demo.Department;

import lombok.*;

import javax.persistence.*;

/*
------------------lombok-----------------
getters,setters,toString, noArgConstructor, ArgConstructor,Equals, HashCode
clean class with lombok
java recer mainly used for immutability and do almost same job as lobok do
class that doesn't require field to be final, we can use lombok there
@Data - arg constr,to string, hashcode , setter and if fields are final then getters also
@Slf4j - for logger
 */
@Entity
@Table
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Department {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(
            updatable = false,
            nullable = false
    )
    private Long departmentId;

    private String departmentName;

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }
}
