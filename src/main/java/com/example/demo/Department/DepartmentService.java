package com.example.demo.Department;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class DepartmentService {

    @Autowired
    private final DepartmentRepository departmentRepository;

    public List<Department> getDepartments() {
        List<Department> departmentList = departmentRepository.findAll();
        return departmentList;
    }

    public void addDepartment(Department department) {
        String departmentName=department.getDepartmentName();

        if(departmentName==null || departmentName.length()==0) {
            throw new IllegalArgumentException("Department Name Can't Be Empty");
        }
        departmentRepository.save(department);
    }

    public void deleteDepartment(Long departmentId) {
        try{
            departmentRepository.deleteById(departmentId);
        } catch (Exception exception){
            throw new IllegalArgumentException("department Id not found");
        }
    }

    public void updateDepartment(Long departmentId, String departmentName) {
        try{
            Department department=departmentRepository.findById(departmentId).get();
            department.setDepartmentName(departmentName);
            departmentRepository.save(department);
        }
        catch(Exception ignored) {
            throw new IllegalArgumentException("department Id not found");
        }
    }
}
