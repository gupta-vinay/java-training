package com.example.demo.Department;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Data
@RequestMapping(path = "api/v/department")
public class DepartmentController {

    @Autowired
    private final DepartmentService departmentService;

    @GetMapping
    public List<Department> getDepartments() {
        return departmentService.getDepartments();
    }

    @PostMapping
    public void addDepartment(@RequestBody Department department) {
        departmentService.addDepartment(department);
    }

    @DeleteMapping(path="{departmentId}")
    public void deleteDepartment(@PathVariable Long departmentId){
        departmentService.deleteDepartment(departmentId);
    }

    @PutMapping(path="{departmentId}")
    public void updateDepartment(@PathVariable Long departmentId,
                                 @RequestParam String departmentName){
        departmentService.updateDepartment(departmentId,departmentName);
    }
}
